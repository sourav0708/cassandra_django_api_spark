from cassandra.cqlengine import connection
from cassandra.cqlengine.management import sync_table
from cassandra.cluster import Cluster
from cassandra.cqlengine.connection import setup


from django.http import HttpResponse
from datetime import datetime
from django.shortcuts import render, render_to_response
from django.http import HttpResponse, HttpResponseRedirect
from django.template import RequestContext
from django.core import urlresolvers
from django.contrib import messages
from django.contrib.auth import authenticate, login as login_auth,logout,get_user
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import json
from datetime import datetime, timedelta,date
import operator
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.csrf import csrf_exempt
import feedparser
import hashlib
from newspaper import Article
import re
import newspaper
import requests
from django.core import serializers
from django.db.models import Q
import random

from  am_spark_app.client_info import CLIENTS
from am_spark_app.models import NewsArticle, Tweet

import logging
logger = logging.getLogger(__name__)

def cassandra_connect():
    setup(['127.0.0.1'], 'spark_ml', retry_connect=True)
    sync_table(NewsArticle)
    
def get_date_string(datetime_obj):
    return datetime.strftime(datetime_obj, '%Y-%m-%dT%H:%M:%S.%fZ')

CATEGORIES = ['entertainment',
 'politics',
 'transport',
 'golf',
 'chess',
 'technology',
 'skiing',
 'hockey',
 'food',
 'economy',
 'election',
 'visa immigration',
 'automobile',
 'volley ball',
 'spirituality',
 'football',
 'space',
 'education',
 'basketball',
 'sports',
 'health',
 'hocky',
 'travel',
 'business',
 'realestate',
 'cricket',
 'tennis',
 'motorsport',
 'rugby',
 'wrestling',
 'money',
 'water polo',
 'fashion',
 'science',
 'formula one',
 'environment',
 'baseball',
 'mutual fund',
 'finance',
 'hollywood',
 'lifestyle',
 'defence',
 'telecom']

def get_url(provider, rss_cat):
    urls = []
    json_data_client = CLIENTS
    provider = json_data_client.get(provider).get('provider')
    end_points = json_data_client.get(provider).get('endpoints').get(rss_cat).split(",")
    if provider in ('bbc', 'dainikbhaskar', 'spiegel'):
        urls = get_url_formatted(json_data_client, provider, rss_cat)
        return urls
    base_url = json_data_client.get(provider).get('base_url')
    for endpoint in end_points[0:1]:
        url = base_url + endpoint
        urls.append(url)
    return urls


def get_url_formatted(json_data_client, provider, rss_cat):
    urls = []
    end_points = json_data_client.get(provider).get('endpoints').get(rss_cat).split(",")
    base_url = json_data_client.get(provider).get('base_url')
    for endpoint in end_points[0:1]:
        url = base_url.format(endpoint)
        urls.append(url)
    return urls


def parse_url(url,provider):
    items = []
    column_list = ['title', 'summary', 'id', 'language', 'link', 'description',
                   'published', 'media_content', 'image']
    elements = feedparser.parse(url)
    elements = [element for element in elements.entries]
    if not elements:
        return []
    for element_res in elements:
        item = {}
        for element in element_res:
            if element in column_list:
                item[element] = element_res.get(element)
                if 'media_content' in element_res:
                    item['image_url'] = element_res.media_content[0]['url'] #Currently getting only for cnn#element_res.media_content[element]['url']
                else:
                    item['image_url'] = element_res.get('image')
        item = get_metadata_newspaper(item,provider)
        if item:
            items.append(item)
    return items


def get_metadata_newspaper(item,provider):
    try:
        if not item['link'] and not item['id']:
            return {}
        if item['link'] and item['link'] !="":
            article = Article(item['link'])
        else:
            article = Article(item['id'])

        try:
            article.download()
            article.parse()
        except newspaper.ArticleException:
            try:
                article = Article(item['link'])
                article.download()
                article.parse()
            except:
                article = {}
        if article:
            if article.title:
                item['title'] = article.title
            if article.text:
                item['description'] = article.text
            if article.top_image:
                item['image_url'] = article.top_image
    except:
        pass

    return item

def update_metadata(items, rss_cat, provider):
    kg_enhancement_file = rss_cat + "_tags.txt"
    results = []
    for it in items:
        title = it.get('title', '')
        description = re.sub('[^a-zA-Z0-9\s ]', '', title
            )
        description = it.get('description', '')
        description = re.sub('<[^<]+?>', '', description)
        description = re.sub('[^a-zA-Z0-9\s ]', '', description)
        if title == '' and description == '':
            continue

        if it.get('published'):
            publish_date = datetime.now()
        else:
                publish_date = datetime.now()
        # Update the dicitonary  RSS provider data_point
        result = {}
        result['item_id'] = hashlib.md5(title.encode('utf-8')).hexdigest()
        result['provider'] = provider
        result['title'] = title
        result['description'] = description
        result['rss_categories'] = rss_cat
        result['image_url'] = it.get('image_url', '')
        result['publish_date'] = publish_date
        result['last_modified'] = datetime.now()
        results.append(result)
    return results


def pull_feeds(provider, rss_cat):
    # get provider from the config file
    urls = get_url(provider, rss_cat)
    for url in urls:
        items = parse_url(url, provider)
        if items:
            results = update_metadata(items, rss_cat, provider)
            store_record(results, provider)
        else:
            logger.info("Unable to pull items for `{}` in  `{}` category".format(provider, rss_cat))

def store_record(results,provider):
    cassandra_connect()
    for data in results:
        title = data.get("title", "")
        description = data.get("description", "")
        rss_categories = data.get("rss_categories", "")
        image_url = data.get("image_url", "")
        item_id = data.get("item_id", "")
        last_modified = get_date_string(datetime.now())
        try:
            NewsArticle.create(
            item_id = item_id,
            title=title,
            description=description,
            rss_categories=rss_categories,
            provider=provider,
            image_url=image_url,
            last_modified=last_modified
            )
            logger.info("successfully saved data : %s provider: %s"  %(data,provider))        
        except:
            logger.error("Error to save article")

def sync_news_articles():
    msg = ""
    try:
        categories = []
        for key, val in CLIENTS.items():
            endpoints = val.get("endpoints", {})
            for categoy, rss in endpoints.items():
                categories.append(categoy)
        rss_categories = list(set(categories))
        json_data_client = CLIENTS
        clients = [
                 'chapmanathletics',
                 'espn',
                 'nba',
                 'cnn',
                 'ncaa',
                 'newindianexpress',
                 'news18',
                 'yahoo',
                 'ndtv',
                 'timesofindia',
                 'indiatoday',
                 'bbc',
                 'zeenews',
                 'hindustantimes',
                 'economictimes',
                 'moneycontrol',
                 'foxnews',
                 ]
        try:
            for client in clients:
                client_endpoints_keys = json_data_client.get(client).get('endpoints').keys()
                for category in rss_categories:
                    if category in client_endpoints_keys:
                        pull_feeds(client, category)
            msg = "Successfully synced"
        except:
            pass
    except:
        msg = "Error in ynced"
        pass
    return msg

def get_news_articles(page_size,category,keyword):
    try:
        page_size = int(page_size)
    except:
        page_size = 10
    final_result = []
    cassandra_connect()
    offset = random.randrange(6, 90)
    query_result = NewsArticle.objects.filter().values_list('title','description','rss_categories','image_url','last_modified').limit(page_size)[offset:offset+page_size]

    # if category and keyword:
    #     query_result = NewsArticle.objects.filter(title__contains=keyword,description__contains=keyword).filter(rss_categories__contains=category).values_list('title','description','rss_categories','image_url','last_modified')
    # elif category:
    #     query_result = NewsArticle.objects.filter(rss_categories=category).values_list('title','description','rss_categories','image_url','last_modified')
    # elif keyword:
    #     query_result = NewsArticle.objects.filter(title__contains=keyword,description__contains=keyword).values_list('title','description','rss_categories','image_url','last_modified')
    # else:
    #     query_result = NewsArticle.objects.filter().values_list('title','description','rss_categories','image_url','last_modified')
    data = list(query_result)
    for counter, record in enumerate(data):
        if counter >= page_size:
            continue ;

        title = record[0]
        description = record[1]
        rss_categories = record[2]
        image = record[3]
        last_modified = record[4]
        if title and description:
            item_inner = {}
            item_inner["title"] = title
            item_inner["description"] = description
            item_inner["image"] = image
            item_inner["category"] = rss_categories
            item_inner["last_modified"] = last_modified
            final_result.append(item_inner)
    return final_result


def news_api(request):
    page_size = request.GET.get("page_size", 10)
    category = request.GET.get("category", "")
    keyword = request.GET.get("keyword", "")

    final_result = get_news_articles(page_size=page_size,category=category,keyword=keyword)
    return_object_final = {"result": final_result, "page_size": len(final_result)}
    return HttpResponse(json.dumps(return_object_final),
            content_type="application/json")

def get_statistics():
    total_no_records = ""
    last_syned = get_date_string(datetime.now())
    try:
        cassandra_connect()
        total_no_records = NewsArticle.objects.count()
    except:
        logger.info("Error in get_statistics")
        pass
    return total_no_records

def index(request):
    """Index page"""
    page_size = 80
    category = ""
    keyword = ""
    # total_no_records = get_statistics()
    final_result = get_news_articles(page_size=page_size,category=category,keyword=keyword)
    return render(request, 'index.html',locals())

def sync_news_articles_api(request):
    msg = "Sync started"
    try:
        logger.info("sync statrted")
        sync_news_articles()
    except:
        msg = "Error in ynced"
        pass
    return HttpResponse(json.dumps({"msg":msg}),
            content_type="application/json")

def get_sync_status_api(request):
    total_no_records = 0
    status = "yes"
    #Due to server limitation commented data base query 
    # try:
        # total_no_records = get_statistics()
        # TOTAL_NO_RECORDS = request.session.get("TOTAL_NO_RECORDS",0)
        # logger.info("TOTAL_NO_RECORDS",TOTAL_NO_RECORDS,"total_no_records",total_no_records)
        # if TOTAL_NO_RECORDS and total_no_records > TOTAL_NO_RECORDS:
        #     status = "yes"
        #     request.session["TOTAL_NO_RECORDS"] = total_no_records
        # else:
        #     status = "no"
        #     request.session["TOTAL_NO_RECORDS"] = total_no_records
    # except:
    #     total_no_records = 0
    #     pass
    return HttpResponse(json.dumps({"is_synced": status}),
            content_type="application/json")

def store_tweet(tweet,user_image):
    cassandra_connect()
    try:
        Tweet.create(
        tweet=tweet,
        user_image=user_image,
        last_modified= get_date_string(datetime.now())
        )
        logger.info("successfully saved tweet : %s user_profile: %s" % (tweet, user_image))        
    except:
        logger.error("Error to save tweet tweet : %s user_profile: %s" %(tweet, user_image))

def get_tweet(page_size=9):
    try:
        page_size = int(page_size)
    except:
        int(page_size)

    final_result = []
    cassandra_connect()
    offset = random.randrange(6, 90)
    query_result = Tweet.objects.filter().values_list('tweet', 'user_image')[offset:offset+page_size]
    data = list(query_result)
    for counter, record in enumerate(data):
        if counter >= page_size:
            continue;
        tweet = record[0]
        user_image = record[1]
        if tweet and user_image:
            item_inner = {}
            item_inner["tweet"] = tweet
            item_inner["user_image"] = user_image
            final_result.append(item_inner)
    return final_result


def tweet_streaming(request):
    """Twittter straming page"""

    return render(request, 'tweet_streaming.html',locals())

def tweet_api(request):
    page_size = request.GET.get("page_size", 9)
    final_result = get_tweet(page_size=page_size)
    return_object_final = {"result": final_result, "page_size": len(final_result)}
    return HttpResponse(json.dumps(return_object_final),
            content_type="application/json")