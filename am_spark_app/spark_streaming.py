from pyspark import SparkContext
from pyspark.streaming import StreamingContext
from pyspark.sql import SQLContext
from pyspark.sql.functions import desc

sc = SparkContext()


ssc = StreamingContext(sc, 30 )
sqlContext = SQLContext(sc)
ssc.checkpoint( "rends/checkpoint")

socket_stream = ssc.socketTextStream("127.0.0.1", 6601)
lines = socket_stream.window( 60 )

from collections import namedtuple
fields = ("tag", "count" )
Tweet = namedtuple( 'Tweet', fields )
