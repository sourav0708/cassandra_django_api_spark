CLIENTS = {
    "chapmanathletics": {"base_url": "http://www.chapmanathletics.com/",
            "endpoints": {
                          "baseball": "sports/bsb/headlines-featured?feed=rss_2.0",
                          "basketball":"sports/mbkb/headlines-featured?feed=rss_2.0,sports/wbkb/headlines-featured?feed=rss_2.0",
                          "rugby":"sports/fball/headlines-featured?feed=rss_2.0",
                          "golf":"sports/mgolf/headlines-featured?feed=rss_2.0",
                          "football":"sports/msoc/headlines-featured?feed=rss_2.0,sports/wsoc/headlines-featured?feed=rss_2.0",
                          "tennis":"sports/mten/headlines-featured?feed=rss_2.0,sports/wten/headlines-featured?feed=rss_2.0",
                          "water polo":"sports/mwaterpolo/headlines-featured?feed=rss_2.0,sports/wwaterpolo/headlines-featured?feed=rss_2.0",
                          "volley ball":"sports/wvball/headlines-featured?feed=rss_2.0"
                        },
            "parsing_type": "feedparser",
            "provider": "chapmanathletics"
            },

    "nba": {"base_url": "http://www.nba.com/rss/",
            "endpoints": {
                          "basketball": "nba_rss.xml",
                        },
            "parsing_type": "feedparser",
            "provider": "nba"
            },

     "espn": {"base_url": "http://www.espn.com/",
            "endpoints": {
                          "basketball": "/espn/rss/nba/news,espn/rss/ncb/news",
                          "rugby": "espn/rss/nfl/news",
                          "baseball": "espn/rss/mlb/news",
                          "hocky": "nhl/rss.xml",
                        },
            "parsing_type": "feedparser",
            "provider": "espn"
            },

     "yahoo": {"base_url": "https://sports.yahoo.com/",
            "endpoints": {
                          "basketball": "/nba/rss.xml",
                          "rugby": "nfl/rss.xml",
                          "baseball": "mlb/rss.xml",
                          "hocky": "nhl/rss.xml",
                          "golf": "golf/rss.xml",
                        },
            "parsing_type": "feedparser",
            "provider": "yahoo"
            },
    "ncaa": {"base_url": "http://www.ncaa.com/",
            "endpoints": {
                          "rugby": "news/football/fbs/rss.xml,news/football/fcs/rss.xml,news/football/d2/rss.xml,news/football/d3/rss.xml",
                          "baseball": "news/baseball/d1/rss.xml,news/baseball/d2/rss.xml,news/baseball/d3/rss.xml,",
                          "basketball": "news/basketball-men/d1/rss.xml,news/basketball-men/d2/rss.xml,news/basketball-men/d3/rss.xml,",
                          "golf": "news/golf-men/d1/rss.xml,news/golf-men/d2/rss.xml,news/golf-men/d3/rss.xmlhockey",
                          "hockey": "news/icehockey-men/d1/rss.xml,news/icehockey-men/d3/rss.xml",
                          "football": "news/soccer-men/d1/rss.xml,news/soccer-men/d2/rss.xml,news/soccer-men/d3/rss.xml",
                          "wrestling":"news/wrestling/d3/rss.xml,news/wrestling/d2/rss.xml,news/wrestling/d1/rss.xml",
                          "water polo":"news/waterpolo-men/nc/rss.xml",
                          "volley ball":"news/volleyball-men/nc/rss.xml,news/volleyball-men/d3/rss.xml",
                          "skiing":"news/skiing/nc/rss.xml"
                        },
            "parsing_type": "feedparser",
            "provider": "ncaa"
            },

    "bbc": {"base_url": "http://feeds.bbci.co.uk/news/{}/rss.xml",
            "endpoints": {
                          "business": "business",
                          "education": "education",
                          "entertainment": "entertainment_and_arts",
                          "health": "health",
                          "politics": "politics",
                          "science": "science_and_environment",
                          "technology": "technology",
                        },
            "parsing_type": "feedparser",
            "provider": "bbc"
            },

     "cnn": {"base_url": "http://rss.cnn.com/rss/",
             "endpoints": {"motorsport": "edition_motorsport.rss",
                           "money": "money_news_international.rss",
                           "entertainment": "edition_entertainment.rss",
                           "travel": "edition_travel.rss",
                           "space": "edition_space.rss",
                           "sports": "edition_sport.rss",
                           "tennis":"edition_tennis.rss",
                           "football":"edition_football.rss",
                           "technology": "edition_technology.rss",
                           "golf":"edition_golf.rss",

                        },
             "parsing_type": "feedparser",
             "provider": "cnn"},

     "foxnews": {"base_url": "http://feeds.foxnews.com/foxnews/",
                 "endpoints": {
                               "entertainment": "entertainment",
                               "health": "health",
                               "lifestyle": "section/lifestyle",
                               "politics": "politics",
                               "science": "science",
                               "sports": "sports",
                               "technology": "tech",
                               "travel":"internal/travel/mixed",
                               },
                 "parsing_type": "feedparser",
                 "provider": "foxnews"},

     "indiatoday": {"base_url": "http://indiatoday.intoday.in/rss/",
                    "endpoints": {
                                 "economy": "article.jsp?sid=34",
                                 "sports": "article.jsp?sid=41",
                              },
                    "parsing_type": "xml",
                    "provider": "indiatoday"},

     "moneycontrol": {"base_url": "http://www.moneycontrol.com/rss/",
                      "endpoints": {
                                    "finance": "pfcolumns.xml,brokeragerecos.xml,buzzingstocks.xml,economy.xml,marketreports.xml,internationalmarkets.xml,marketedge.xml,marketoutlook.xml,iponews.xml,insurancenews.xml",
                                    "mutual fund":"mfcolumns.xml,mfnews.xml",
                                    "entertainment": "entertainment.xml",
                                    "sports": "sports.xml",
                                    "technology": "technology.xml",
                                    "business":"business.xml"
                                   },
                      "parsing_type": "feedparser",
                      "provider": "moneycontrol"},
     "ndtv": {"base_url": "https://feeds.feedburner.com/",
              "endpoints": {"automobile": "carandbike-latest",
                            "business": "ndtvprofit-latest",
                            "entertainment": "ndtvmovies-latest",
                            "health": "ndtvcooks-latest",
                            "lifestyle": "ndtvnews-offbeat-news",
                            "sports": "ndtvsports-latest",
                            "cricket":"ndtvsports-cricket",
                            "technology": "gadgets360-latest",
                           },
              "parsing_type": "feedparser",
              "provider": "ndtv"},

     "news18": {"base_url": "http://www.news18.com/rss/",
                "endpoints": {
                             "business": "business.xml",
                             "entertainment": "movies.xml",
                             "lifestyle": "lifestyle.xml",
                             "politics": "politics.xml",
                             "sports": "sports.xml",
                             "technology": "tech.xml",
                             "education":"books.xml",
                             "cricket":"cricketnext.xml",
                             "football":"football.xml",
                             "hockey":"hockey.xml",
                             "formula one":"formula-one.xml"
                            },
                "parsing_type": "feedparser",
                "provider": "news18"},


     "reuters": {"base_url": "http://feeds.reuters.com/reuters/",
                 "endpoints": {
                             "formula one": "INformulaOne",
                             "business": "INbusinessNews",
                             "entertainment": "INentertainmentNews",
                             "health": "INhealth",
                             "lifestyle": "INlifestyle",
                             "sports": "INsportsNews",
                             "technology": "INtechnologyNews",
                             "cricket":"INcricketNews",
                             "hollywood":"INhollywood",
                             "golf":"INgolf",
                             },
                 "parsing_type": "feedparser",
                 "provider": "reuters"},

     "timesofindia": {"base_url": "http://timesofindia.indiatimes.com/rssfeeds/",
                      "endpoints": {
                                     "business": "1898055.cms",
                                     "education": "913168846.cms",
                                     "entertainment": "1081479906.cms",
                                     "health": "3908999.cms",
                                     "lifestyle": "2886704.cms",
                                     "science": "-2128672765.cms",
                                     "sports": "4719148.cms",
                                     "technology": "5880659.cms",
                                     "cricket":"4719161.cms",
                                     "environment":"2647163.cms",
                                    },
                      "parsing_type": "feedparser",
                      "provider": "timesofindia"},

     "zeenews": {"base_url": "http://zeenews.india.com/rss/",
                 "endpoints": {
                                "business": "business.xml",
                                "entertainment": "entertainment-news.xml",
                                "health": "health-news.xml",
                                "sports": "sports-news.xml",
                                "technology": "technology-news.xml",
                                "science":"science-environment-news.xml",
                                },
                 "parsing_type": "feedparser",
                 "provider": "zeenews"},

      "newindianexpress": {"base_url": "http://www.newindianexpress.com/",
                 "endpoints": {
                                "cricket": "Sport/Cricket/rssfeed/?id=188&getXmlFeed=true,Sport/IPL/rssfeed/?id=284&getXmlFeed=true",
                                "tennis": "Sport/Tennis/rssfeed/?id=189&getXmlFeed=true",
                                "football": "http://www.newindianexpress.com/Sport/Football/rssfeed/?id=190&getXmlFeed=true",
                                "sports": "Sport/Other/rssfeed/?id=191&getXmlFeed=true",
                                "entertainment": "Entertainment/English/rssfeed/?id=194&getXmlFeed=true,Entertainment/Hindi/rssfeed/?id=195&getXmlFeed=true,Entertainment/Kannada/rssfeed/?id=196&getXmlFeed=true,Entertainment/Malayalam/rssfeed/?id=197&getXmlFeed=true,Entertainment/Tamil/rssfeed/?id=198&getXmlFeed=true,Entertainment/Telugu/rssfeed/?id=320&getXmlFeed=true,Entertainment/Review/rssfeed/?id=199&getXmlFeed=true",
                                "technology": "Life-Style/Tech/rssfeed/?id=212&getXmlFeed=true",
                                "health": "Life-Style/Health/rssfeed/?id=213&getXmlFeed=true",
                                "travel": "Life-Style/Travel/rssfeed/?id=214&getXmlFeed=true",
                                "food": "Life-Style/Food/rssfeed/?id=215&getXmlFeed=true",
                                "spirituality":"Life-Style/Spirituality/rssfeed/?id=217&getXmlFeed=true,Life-Style/Spirituality/Astrology/rssfeed/?id=218&getXmlFeed=true",
                                "education":"Education/Edex/rssfeed/?id=229&getXmlFeed=true,Life-Style/Books/rssfeed/?id=216&getXmlFeed=true",
                                "automobile":"Auto/rssfeed/?id=210&getXmlFeed=true",
                              },
                "parsing_type": "feedparser",
                "provider": "newindianexpress"},

      "hindustantimes": {"base_url": "http://www.hindustantimes.com/rss/",
                 "endpoints": {
                                "cricket": "cricket/rssfeed.xml",
                                "tennis": "tennis/rssfeed.xml",
                                "football": "football/rssfeed.xml",
                                "sports": "othersports/rssfeed.xml",
                                "entertainment": "entertainment/rssfeed.xml,movie-reviews/rssfeed.xml,bollywood/rssfeed.xml,hollywood/rssfeed.xml,regional-movies/rssfeed.xml,world-cinema/rssfeed.xml,tv/rssfeed.xml,music/rssfeed.xml,tabloid/rssfeed.xml",
                                "technology": "tech/rssfeed.xml,tech-features/rssfeed.xml,tech-gadgets/rssfeed.xml,tech-news/rssfeed.xml",
                                "health": "health-fitness/rssfeed.xml,sex-relationships/rssfeed.xml",
                                "travel": "travel/rssfeed.xml",
                                "fashion": "fashion-trends/rssfeed.xml",
                                "lifestyle":"lifestyle/rssfeed.xml",
                                "education":"books/rssfeed.xml,education/rssfeed.xml",
                                "automobile":"auto/rssfeed.xml",
                                "business":"business/rssfeed.xml",
                                "realestate":"realestate/rssfeed.xml"
                              },
                "parsing_type": "feedparser",
                "provider": "hindustantimes"},

      "rediff": {"base_url": "http://www.rediff.com/rss/",
                 "endpoints": {
                                "entertainment": "moviesrss.xml,movieshollynewsrss.xml,moviesinterrss.xml,moviesreviewsrss.xml,moviesssnewsrss.xml,moviestvnewsrss.xml,moviesbollynewsrss.xml,moviescolumnrss.xml,columnrss.xml,mslide.xml",
                                "election": "electiongraphics.xml,election.xml,electioncolumns.xml,electioninterviews.xml,electionnews.xml,electionspecials.xml,electionvvoice.xml",
                                "sports": "sportsrss.xml,sspotted.xml",
                                "chess":"schess.xml",
                                "formula one":"sf1.xml",
                                "hockey":"shockey.xml",
                                "cricket": "cricketrss.xml,ccolumnrss.xml,cinterrss.xml,cspotted.xml,cstatsrss.xml,cslide.xml,sslide.xml,cricketrss.xml",
                                "tennis": "stennis.xml",
                                "football": "sfootball.xml",
                                "finance":"perfinrss.xml,getaheadmoneyrss.xml",
                                "business":"moneyrss.xml,commodrss.xml,bslide.xml,moneyrss.xml",
                              },
                "parsing_type": "feedparser",
                "provider": "rediff"},

    "economictimes": {"base_url": "http://economictimes.indiatimes.com/",
                 "endpoints": {
                                "technology": "tech/rssfeeds/13357270.cms,industry/tech/rssfeeds/56811438.cms,small-biz/entrepreneurship/rssfeeds/11993034.cms,small-biz/startups/rssfeeds/11993050.cms,tech/hardware/rssfeeds/13357565.cms,tech/software/rssfeeds/13357555.cms,tech/internet/rssfeeds/13357549.cms,tech/ites/rssfeeds/40274504.cms,small-biz/security-tech/rssfeeds/47280820.cms",
                                "mutual fund":"mf/rssfeeds/359241701.cms",
                                "defence":"news/defence/rssfeeds/46687796.cms,industry/defence/rssfeeds/58571556.cms",
                                "science":"news/science/rssfeeds/39872847.cms",
                                "environment":"news/environment/rssfeeds/2647163.cms,industry/energy/rssfeeds/13358350.cms,industry/environment/rssfeeds/58571602.cms",
                                "politics":"news/politics-and-nation/rssfeeds/1052732854.cms",
                                "sports": "news/sports/rssfeeds/26407562.cms,industry/sports/rssfeeds/58571631.cms",
                                "automobile": "industry/auto/rssfeeds/13359412.cms",
                                "transport":"industry/transportation/rssfeeds/13353990.cms",
                                "finance":"nri/nri-tax/rssfeeds/7771292.cms,wealth/tax/rssfeeds/47119912.cms,nri/nri-investments/rssfeeds/7771289.cms,nri/forex-and-remittance/rssfeeds/7771285.cms,industry/banking/finance/rssfeeds/13358259.cms",
                                "health":"industry/healthcare/biotech/rssfeeds/13358050.cms",
                                "telecom":"industry/telecom/rssfeeds/13354103.cms",
                                "entertainment": "industry/media/entertainment/rssfeeds/13357212.cms",
                                "travel":"magazines/travel/rssfeeds/640246854.cms",
                                "realestate":"nri/nri-real-estate/rssfeeds/7771290.cms,wealth/real-estate/rssfeeds/48997582.cms",
                                "visa immigration":"nri/visa-and-immigration/rssfeeds/7771304.cms",
                              },
                "parsing_type": "feedparser",
                "provider": "economictimes"},


}

                                                                                                                                                                                                                                      