import uuid
from cassandra.cqlengine import columns
from cassandra.cqlengine.models import Model
from datetime import datetime

class NewsArticle(Model):
    read_repair_chance = 0.05
    article_id = columns.UUID(default=uuid.uuid4,primary_key=True)
    item_id = columns.Text(required=True)
    title = columns.Text(required=True)
    description = columns.Text(required=True)
    provider = columns.Text(required=False)
    rss_categories = columns.Text(required=False)
    image_url = columns.Text(required=False)
    last_modified = columns.Text(required=False)

class Tweet(Model):
    read_repair_chance = 0.05
    tweet_id = columns.UUID(default=uuid.uuid4, primary_key=True)
    tweet = columns.Text(required=True)
    user_image = columns.Text(required=False)
    last_modified = columns.Text(required=False)