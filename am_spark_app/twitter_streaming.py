import tweepy
from tweepy import OAuthHandler
from tweepy import Stream
from tweepy.streaming import StreamListener
import socket
import json
import time
import re
from am_spark_app.views import store_tweet

consumer_key = "bZZGYjvJUofatDb5Oh5LXJzBz"
consumer_secret = "svq1wXiOXTgHl3DEN3kjSQgCsE6SVigfaftNvWKs1fKdEMoQya"
access_token = "936829983950909441-JoAW6l92IbcumDrpLu62dYDDslt7UfT"
access_secret = "VHwrqr17K2S2eMiCn85e4XAXhkoxRBDw2ARIL2E3prWFX"

import logging
logger = logging.getLogger(__name__)

class TweetsListener(StreamListener):

  def __init__(self, csocket):
      self.client_socket = csocket
      self.counter = 0

  def on_data(self, data):
    try:
      msg = json.loads( data )
      text = msg.get('text','').encode('utf-8')
      user_image = msg.get("user",{}).get('profile_image_url','')
      # clean_text = re.sub("[^a-zA-Z0-9\s\#\.]", ' ',  msg.get('text','') )
      # if len(clean_text) >= 5:
      self.counter += 1
      store_tweet(msg.get('text',''), user_image)
      if self.counter >= 300:
        return False
      self.client_socket.send(text)
      return True
    except BaseException as e:
      logger.error("Error on_data: %s" % str(e))
      return True

  def on_error(self, status):
      return_obj = {}
      logger.error("Twitter streaming error",status)
      return True

def sendData(c_socket, keywords=["india","kolkata","code data"]):
  auth = OAuthHandler(consumer_key, consumer_secret)
  auth.set_access_token(access_token, access_secret)
  twitter_stream = Stream(auth, TweetsListener(c_socket))
  twitter_stream.filter(track=keywords)

def start_streaming(port=6601,keywords =["soccer","football","kolkata"]): 
  s = socket.socket()         # Create a socket object
  host = "127.0.01"      # Get local machine name
  s.bind((host, port))        # Bind to the port
  logger.info("Listening on port: %s" % str(port))
  s.listen(5)                 # Now wait for client connection.
  c, addr = s.accept()        # Establish connection with client.
  logger.info( "Received request from: " + str( addr ) )
  sendData( c, keywords)

def streaming_client(port=6601):
  # create a socket object
  s = socket.socket(socket.AF_INET, socket.SOCK_STREAM) 
  # get local machine name
  host = '127.0.0.1'                          
  # connection to hostname on the port.
  s.connect((host, port))                               
  # Receive no more than 1024 bytes
  message = s.recv(1024)
  logger.info("The data got from the server is %s" % message)
