"""am_spark_proj URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
# from django.conf.urls import patterns, include, url
from django.conf.urls import url
from django.contrib import admin

from django.conf import settings
admin.site.site_header = 'Django - SparkML - Cassandra'

from am_spark_app import views as spark_views

urlpatterns = [
    url(r'^news-api', spark_views.news_api, name='news_api'),
    url(r'^sync-news', spark_views.sync_news_articles_api, name='sync-news'),
    url(r'^get-sync-status', spark_views.get_sync_status_api, name='get-sync-status'),
    url(r'^tweet-api', spark_views.tweet_api, name='tweet-api'),
    url(r'^admin', admin.site.urls),
    url(r'^news', spark_views.index, name='index'),
    url(r'^', spark_views.tweet_streaming, name='tweet-streaming'),
    ]
