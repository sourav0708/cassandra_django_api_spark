//********************************* STARTs HERE ***********
//For User input form validation error message
var message;
function commonPnotify(type) {
    var opts = {
        shadow: false
    };
    switch (type) {
        case 'startSynInfo':
            opts.title ="Info :)";
            opts.text = "Sync has been started !!!";
            opts.type = "info";
            break;
        
        case 'syncSuccess':
            opts.title ="Success :)";
            opts.text = "News has been synced successfully !!!";
            opts.type = "info";
            break;

        case 'syncNoUpdate':
            opts.title ="Info :)";
            opts.text = "No new news since last sync !!!";
            opts.type = "danger";
            break;
        

     }
     new PNotify(opts);
  }


  var news_articles_table_datatable = $("table#news_articles_table").dataTable({
           keys: true,
           fixedHeader: true,
           sPaginationType: "full_numbers",
           iDisplayLength: 10,
           ordering: false,
           oLanguage: {
                sSearch: "Search",
          },
          dom: "Bfrtip",
                buttons: [{
                  extend: "copy",
                  className: "btn-sm btn-datatable btn-dark",
                  title:"News Articles",
                  text:"<i class='fa fa-copy' aria-hidden='true'></i>",
                }, {
                  extend: "csv",
                  className: "btn-sm  btn-datatable btn-success",
                  title:"News Articles",
                  text:"<i class='fa fa-file' aria-hidden='true'></i>",
                }, {
                  extend: "excel",
                  className: "btn-sm btn-datatable btn-info",
                  title:"News Articles",
                  text:"<i class='fa fa-file-excel-o' aria-hidden='true'></i>",
                }, {
                  extend: "pdf",
                  className: "btn-sm btn-datatable btn-primary",
                  title:"News Articles",
                  orientation: 'landscape',
                  pageSize: 'LEGAL',
                  text:"<i class='fa fa-file-pdf-o' aria-hidden='true'></i>",
                }, {
                  extend: "print",
                  className: "btn-sm btn-datatable  btn-warning",
                  title:"News Articles",
                  text:"<i class='fa fa-print' aria-hidden='true'></i>",
                }],
 
        });

news_articles_table_datatable.columnFilter({ 
                sPlaceHolder: "head:before", 
                aoColumns: [ 
                        { type : 'text'},
                        { type : 'text'},
                        ,
                        { type : 'text'},
                        // ,
                         
                ] 
        });



$(document).ready(function(){


    $('[data-toggle="tooltip"]').tooltip();
    $("#sync_news_article").click(function(){
          commonPnotify('startSynInfo');
          $("#loadingSpinner").show();
              $.ajax({
              url : "/sync-news/", 
              type : "GET", 
              timeout: '60000',
              data : {}, 
              success : function(data) {
                $("#loadingSpinner").hide();
                var search_result = data
                // console.log(search_result);
                  },
              error : function(xhr,errmsg,err) {
                   console.log(xhr.status + ": " + xhr.responseText+"xhr"+xhr+"err"+err);
                   $("#loadingSpinner").hide();
              }
          });
      });

});



